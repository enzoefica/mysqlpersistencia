package ar.com.hibernet.main;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import ar.com.hibernet.hibernetUtil.HibernetUtil;
import ar.com.hibernet.models.Cliente;
import ar.com.hibernet.models.Congelado;
import ar.com.hibernet.models.Cuenta;
import ar.com.hibernet.models.Detalle;
import ar.com.hibernet.models.Factura;
import ar.com.hibernet.models.Frio;
import ar.com.hibernet.models.General;
import ar.com.hibernet.models.Gondola;
import ar.com.hibernet.models.Precio;
import ar.com.hibernet.models.Producto;
import ar.com.hibernet.models.Proveedor;

public class Main {

	public static void main(String[] args) {
		Cuenta cuenta1 = new Cuenta("236");

		Cliente cliente1 = new Cliente("563", "Enersto", "Silva", cuenta1);
		Cliente cliente2 = new Cliente("566", "Marcia", "Riva", cuenta1);

		Factura factura1 = new Factura(new Date(), 123);
		Factura factura2 = new Factura(new Date(), 132);

		Detalle detalle1 = new Detalle(123);
		Detalle detalle2 = new Detalle(132);
		Detalle detalle3 = new Detalle(123);
		Detalle detalle4 = new Detalle(132);

		Proveedor proveedor1 = new Proveedor("Proveedor de productos 1");
		Proveedor proveedor2 = new Proveedor("Proveedor de productos 2");

		Precio precio1 = new Precio(new BigDecimal("200"), new Date());
		Precio precio2 = new Precio(new BigDecimal("3000"), new Date());
		
		Frio frio1 = new Frio("123", "Cerveza", LocalDate.parse("2022-05-22"), 10, 2, LocalDate.now());
		Frio frio2 = new Frio("124", "Yogurt", LocalDate.parse("2022-05-23"), 10, 2, LocalDate.now());
		
		Congelado congelado1 = new Congelado("325", "Espinaca", LocalDate.parse("2022-05-29"));
		Congelado congelado2 = new Congelado("326", "Helado", LocalDate.parse("2022-05-30"));
		
		Gondola gondola1 = new Gondola("412", "Fideo", LocalDate.parse("2022-05-30"), 500);
		Gondola gondola2 = new Gondola("415", "Mayonesa", LocalDate.parse("2022-05-10"), 250);
		
		General general1 = new General("560", "Zapallo anco", 450);
		General general2 = new General("561", "Zapallo Verde", 200);

		frio1.getProveedores().add(proveedor1);
		frio2.getProveedores().add(proveedor2);
		
		congelado1.getProveedores().add(proveedor1);
		congelado2.getProveedores().add(proveedor2);
		
		gondola1.getProveedores().add(proveedor1);
		gondola2.getProveedores().add(proveedor2);
		
		general1.getProveedores().add(proveedor1);
		general2.getProveedores().add(proveedor2);

		frio1.setPrecio(precio1);
		frio2.setPrecio(precio2);
		
		congelado1.setPrecio(precio1);
		congelado2.setPrecio(precio2);
		
		gondola1.setPrecio(precio1);
		gondola2.setPrecio(precio2);
		
		general1.setPrecio(precio1);
		general2.setPrecio(precio2);

		cliente1.getFacturas().add(factura2);
		cliente2.getFacturas().add(factura1);

		factura1.getDetalles().add(detalle1);

		factura1.setCliente(cliente2);
		
		factura2.getDetalles().add(detalle2);
		
		factura2.setCliente(cliente1);

		detalle1.setFactura(factura1);

		detalle1.setProducto(frio1);

		detalle1.setPrecio(precio1);
		
		detalle2.setFactura(factura2);
		
		detalle2.setProducto(gondola1);
		
		detalle2.setPrecio(precio2);
		
		detalle3.setFactura(factura2);
		
		detalle3.setProducto(general2);
		
		detalle3.setPrecio(precio2);
		
		detalle4.setFactura(factura1);
		
		detalle4.setProducto(congelado1);
		
		detalle4.setPrecio(precio1);
		
	
		frio2.cargarPrecio(precio2);
		frio1.cargarPrecio(precio1);
		
		congelado1.cargarPrecio(precio2);
		congelado2.cargarPrecio(precio1);
		
		gondola1.cargarPrecio(precio2);
		gondola2.cargarPrecio(precio1);
		
		general1.cargarPrecio(precio2);
		general2.cargarPrecio(precio1);

		Session session = HibernetUtil.getSessionFactory().getCurrentSession();
		// session.beginTransaction();

		try {
			session.beginTransaction();
			session.persist(cliente1);
			session.persist(cliente2);
			session.persist(cuenta1);
			session.persist(factura1);
			session.persist(factura2);
			session.persist(detalle1);
			session.persist(detalle2);
			session.persist(detalle3);
			session.persist(detalle4);
			session.persist(proveedor1);
			session.persist(proveedor2);
			session.persist(precio1);
			session.persist(precio2);
			session.persist(frio1);
			session.persist(frio2);
			session.persist(gondola1);
			session.persist(gondola2);
			session.persist(congelado1);
			session.persist(congelado2);
			session.persist(general1);
			session.persist(general2);

			session.getTransaction().commit();
			
			Query query = session.createQuery("from Producto");
			
			@SuppressWarnings("unchecked")
			List<Producto> productos = query.getResultList();
			
			productos.forEach( p -> System.out.println(p.toString()));
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		 //HibernetUtil.getSessionFactory().close();

	}

}
