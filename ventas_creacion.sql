DROP DATABASE ventas;
CREATE DATABASE ventas;
USE ventas;

CREATE TABLE IF NOT EXISTS cliente(
	id_clienteid_clientecodigoapellidonombre INT PRIMARY KEY,
	codigo VARCHAR(16),
	apellido VARCHAR(256),
	nombre VARCHAR(256)
);

CREATE TABLE IF NOT EXISTS factura(
	id_factura INT, 
    id_cliente INT,
	fecha DATETIME,
    numero INT,
    PRIMARY KEY(id_factura, id_cliente),
    FOREIGN KEY(id_cliente) REFERENCES cliente(id_cliente)
    );
    
CREATE TABLE IF NOT EXISTS producto(
	id_producto INT,
    codigo VARCHAR(256),
    descripcion VARCHAR(256),
    id_precio INT,
    PRIMARY KEY(id_producto)
);
  
CREATE TABLE precio(
	id_precio INT,
    monto DECIMAL(8.2),
    fecha DATE,
    id_producto INT,
    PRIMARY KEY(id_precio),
    FOREIGN KEY(id_producto) REFERENCES producto(id_producto)
);

ALTER TABLE producto ADD CONSTRAINT FOREIGN KEY (id_precio) REFERENCES precio(id_precio);

CREATE TABLE IF NOT EXISTS factura_producto(
	id_factura INT,
    id_producto INT,
    cantidad INT,
    PRIMARY KEY(id_factura, id_producto),
    FOREIGN KEY(id_factura) REFERENCES factura(id_factura),
    FOREIGN KEY(id_producto) REFERENCES producto(id_producto)
);






