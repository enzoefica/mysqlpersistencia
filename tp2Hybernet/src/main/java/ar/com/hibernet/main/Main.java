package ar.com.hibernet.main;

import java.util.Date;

import org.hibernate.Session;

import ar.com.hibernet.hibernetUtil.HibernetUtil;
import ar.com.hibernet.models.Cliente;
import ar.com.hibernet.models.Cuenta;
import ar.com.hibernet.models.Detalle;
import ar.com.hibernet.models.Factura;
import ar.com.hibernet.models.Precio;
import ar.com.hibernet.models.Producto;
import ar.com.hibernet.models.Proveedor;

public class Main {

	public static void main(String[] args) {
		Cuenta cuenta1 = new Cuenta("236");

		Cliente cliente1 = new Cliente("563", "Enersto", "Silva", cuenta1);
		Cliente cliente2 = new Cliente("566", "Marcia", "Riva", cuenta1);

		Factura factura1 = new Factura(new Date(), 123);

		Detalle detalle1 = new Detalle(123);

		Proveedor proveedor1 = new Proveedor("Proveedor de productos 1");
		Proveedor proveedor2 = new Proveedor("Proveedor de productos 2");

		Producto producto1 = new Producto("123", "Yogurt Serenisima");
		Producto producto2 = new Producto("126", "Azucar Ledesma");
		Producto producto3 = new Producto("121", "Mayonesa Natura");

		Precio precio1 = new Precio(200, new Date());
		Precio precio2 = new Precio(300, new Date());

		producto1.getProveedores().add(proveedor1);
		producto2.getProveedores().add(proveedor2);
		producto3.getProveedores().add(proveedor1);
		producto3.getProveedores().add(proveedor2);

		producto1.setPrecio(precio1);
		producto2.setPrecio(precio2);

		cliente1.getFacturas().add(factura1);
		cliente2.getFacturas().add(factura1);

		factura1.getDetalles().add(detalle1);

		factura1.setCliente(cliente2);

		detalle1.setFactura(factura1);

		detalle1.setProducto(producto3);

		detalle1.setPrecio(precio1);

		producto1.cargarPrecio(precio2);

		Session session = HibernetUtil.getSessionFactory().getCurrentSession();
		// session.beginTransaction();

		try {
			session.beginTransaction();
			session.persist(cliente1);
			session.persist(cliente2);
			session.persist(cuenta1);
			session.persist(factura1);
			session.persist(detalle1);
			session.persist(proveedor1);
			session.persist(proveedor2);
			session.persist(producto1);
			session.persist(producto2);
			session.persist(producto3);
			session.persist(precio1);
			session.persist(precio2);

			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.close();
		}

		// HibernateUtil.getSessionFactory().close();

	}

}
